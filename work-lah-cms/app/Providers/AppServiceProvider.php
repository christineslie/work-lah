<?php

namespace App\Providers;

// use Firebase\Auth\Token\Verifier;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->singleton(Verifier::class, function ($app) {
        //     return new Verifier('work-lah');
        // });
    }
}

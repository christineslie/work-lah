<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Handle;
use App\Http\Controllers\Controller;
use App\Traits\Firebase;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use Firebase;

    private $tableName = 'companies';

    /**
     * Direct to register view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Store a new user instance after a valid registration.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string'
        ]);

        try {
            $dataFirebase = [
                'companyName' => $request->input('name'),
                'email' => $request->input('email'),
                'location' => '',
                'logo' => '',
                'coordinates' => [
                    'longitude' => '',
                    'latitude' => '',
                ],
            ];

            $uuid = $this->getFirebaseTable($this->tableName)->push($dataFirebase);
            $uuid = $uuid->getKey();

            $dataUser = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'uuid' => $uuid
            ];

            User::create($dataUser);

        } catch (Exception $e) {
            Handle::error('Failed register', $e, $request);
            return redirect('register');
        }

        Handle::success('Successfully registered', $request);
        return redirect('login');
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Handle;
use App\Traits\Firebase;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    use Firebase;

    private $tableName = 'companies/';

    /**
     * Show profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $uuid = Auth::user()->uuid;
        $company = $this->getFirebaseTable($this->tableName . $uuid)->getValue();

        return view('profile', compact(['company', 'uuid']));
    }

    /**
     * Update profile
     *
     * @param $request
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $uuid)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'location' => 'required|string',
            'latitude' => 'required|string',
            'longitude' => 'required|string',
            'existedImage' => 'string'
        ]);

        $data = [
            'companyName' => $request->input('name'),
            'email' => $request->input('email'),
            'location' => $request->input('location'),
            'logo' => (!$request->input('imagePath')) ? $request->input('existedImage') : $request->input('imagePath'),
            'coordinates' => [
                'latitude' => $request->input('latitude'),
                'longitude' => $request->input('longitude')
            ]
        ];

        try {
            $this->getFirebaseReference()->update([
                $this->tableName . $uuid => $data
            ]);

        } catch (Exception $e) {
            Handle::error('Failed updating profile', $e, $request);
            return redirect('profile');
        }

        Handle::success('Successfully updated profile', $request);
        return redirect('profile');
    }
}

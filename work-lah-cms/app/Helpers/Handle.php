<?php

namespace App\Helpers;

use Illuminate\Http\Request;

class Handle
{
    public static function success($msg, Request $request)
    {
        $request->session()->flash('status', 'success');
        $request->session()->flash('msg', $msg);
    }

    public static function error($msg, $ex, Request $request)
    {
        $request->session()->flash('status', 'fail');
        $request->session()->flash('msg', $msg);
    }
}

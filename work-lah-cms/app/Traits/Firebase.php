<?php

namespace App\Traits;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

trait Firebase
{
    /** 
     * Get Service Account from JSON file
     * 
     * @return mixed|ServiceAccount
     */
    protected function getServiceAccount()
    {
        return ServiceAccount::fromJsonFile(__DIR__ . '/work-lah-firebase-adminsdk-u6kaj-99ad88ae6d.json');
    }

    /**
     * Get database
     * 
     * @return mixed|Database
     */
    public function getFirebaseDatabase()
    {
        $serviceAccount = $this->getServiceAccount();

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri(config('services.firebase.database_url'))
            ->create();

        $database = $firebase->getDatabase();

        return $database;
    }

    /**
     * Get table
     * 
     * @param string $table
     * @return mixed
     */
    public function getFirebaseTable($table)
    {
        $database = $this->getFirebaseDatabase();

        return $database->getReference($table);
    }

    /**
     * Get reference only
     * 
     * @return mixed
     */
    public function getFirebaseReference()
    {
        $database = $this->getFirebaseDatabase();

        return $database->getReference();
    }

}

@extends('layouts.app')

@section('title', 'Edit Job')

@section('stylesheets')
@endsection

@section('breadcrumb')
<h5 class="breadcrumbs-title">Edit Job</h5>
<ol class="breadcrumbs">
    <li><a href="{{ route('job.index') }}">Jobs</a></li>
	<li><a href="#">Edit Job</a></li>
</ol>
@endsection

@section('content')
<p class="caption">Job's detail will attract more professional applicant.</p>
<div class="divider"></div>

<div class="row">
	<div class="col s12 m12 l12">
        <div class="card-panel">
        	<div class="row">
            	<form id="profile-form" action="{{ url('job/' . $id) }}" class="col s12" method="POST"  enctype="multipart/form-data">
            		@csrf
            		{{ method_field('PUT') }}

	            	<div class="row">
	                	<div class="input-field col s12">
	                		<i class="mdi-social-location-city prefix"></i>
	                		<input id="role" name="role" type="text" class="validate" value="{{ $job['role'] }}" required>
	                		<label for="role">Role</label>
	                	</div>
	              	</div>
	              	<div class="row">
	                	<div class="input-field col s12">
	                  		<i class="mdi-communication-email prefix"></i>
	                  		<input id="category" name="category" type="text" class="validate" value="{{ $job['category'] }}" required>
	                  		<label for="category">Category</label>
	                	</div>
	              	</div>
	              	<div class="row">
	                	<div class="input-field col s12">
	                		<i class="mdi-maps-map prefix"></i>
	                		<input id="salary" name="salary" type="number" class="validate" value="{{ $job['salary'] }}" required>
	                		<label for="salary">Salary</label>
	                	</div>
	              	</div>
	              	<div class="row">
	                	<div class="input-field col s12">
	                		<i class="mdi-maps-pin-drop prefix"></i>
	                		<input id="type" name="type" type="text" class="validate" value="{{ $job['type'] }}" required>
	                		<label for="type">Type</label>
	                	</div>
	              	</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="mdi-maps-pin-drop prefix"></i>
							<textarea id="description" name="description" type="text" class="materialize-textarea validate" required>{{ $job['description'] }}</textarea>
							<label for="description">Description</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="mdi-maps-pin-drop prefix"></i>
							<input id="responsibilities" name="responsibility" type="text" class="validate" required>
							<label for="responsibilities">Responsibilities</label>
						</div>
					</div>
	                <div class="row">
	                  	<div class="input-field col s12">
	                    	<button class="btn cyan waves-effect waves-light right" type="button" id="updateBtn" name="action">Update
	                      		<i class="mdi-action-input right"></i>
	                    	</button>
	                  	</div>
	                </div>
	            </div>
	        </form>
	    </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('#menu-jobs').addClass('active')
	})
</script>
@endsection
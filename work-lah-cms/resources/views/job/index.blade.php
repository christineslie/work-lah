@extends('layouts.app')

@section('title', 'Job List')

@section('stylesheets')
<link href="{{ asset('js/plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('js/plugins/sweetalert/dist/sweetalert.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('breadcrumb')
<h5 class="breadcrumbs-title">Jobs</h5>
<ol class="breadcrumbs">
    <li><a href="{{ route('job.index') }}">Jobs</a></li>
</ol>
@endsection

@section('content')
<p class="caption">Job List</p>
<div class="divider"></div>
<div id="table-datatables">
	<div class="row">
		<div class="col s12 m12 l12">
			<table id="data-table-simple" class="responsive-table display hoverable" cellspacing="0">
				<thead>
				<tr>
					<th>Role</th>
					<th>Category</th>
					<th>Salary</th>
					<th>Date Posted</th>
					<th>Type</th>
					<th>Actions</th>
				</tr>
				</thead>

				<tbody>
				@foreach($jobs as $key => $job)
					<tr>
						<td>{{ $job['role'] }}</td>
						<td>{{ $job['category'] }}</td>
						<td>{{ $job['salary'] }}</td>
						<td>{{ $job['datePosted'] }}</td>
						<td>{{ $job['type'] }}</td>
						<td>
							<a href="{{ route('job.edit', $key) }}" class="tooltipped edit-row col l2" data-position="top" data-delay="50" data-tooltip="Edit"><i class="mdi-editor-mode-edit"></i></a>
							<a href="#" class="tooltipped remove-row col l2" data-toggle="tooltip" data-id="{{ $key }}" data-name="{{ $job['role'] }}" data-position="top" data-delay="50" data-tooltip="Delete"><i class="mdi-action-delete"></i></a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div style="display: none">
		<form id="delete-form" method="POST">
			@csrf
			{{ method_field('delete') }}
		</form>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/data-tables/data-tables-script.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>

<script>
	$(function() {
		$('#menu-jobs').addClass('active')

		$('.tooltipped').tooltip({
			delay: 50
		})

		$('#data-table-simple tbody').on('click', '.remove-row', function (e) {
			e.preventDefault();
			let id = $(this).data('id');
			let name = $(this).data('name');

			swal({
				title: "Delete Confirmation",
				text: "Do you want to delete " + name + "?",
				showCancelButton: true,
				confirmButtonText: "Yes",
				cancelButtonText: "No"
			}).then(function () {
				$('#delete-form').attr('action', '{{ route("job.index") }}/' + id).submit();
			}).catch(swal.noop);
		});
	})
</script>
@endsection
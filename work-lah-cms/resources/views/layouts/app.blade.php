<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.0
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | Worklah Company Panel</title>

    <!-- Favicon-->
    <link rel="icon" href="{{ asset('images/logo-worklah.png') }}">

    <!-- CORE CSS-->
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    @yield('stylesheets')

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
</head>

<body>
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <ul class="left" style="padding: 1rem;">
                    <li><h1 class="logo-wrapper">
                        <a href="{{ route('home') }}" class="brand-logo darken-1" style="padding: 0;"><img src="{{ asset('images/logo-worklah-white.png') }}" alt="Worklah" style="height: 40px; width: auto;"></a>
                    </h1></li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
</header>
<!-- END HEADER -->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
            <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-1">
                    <div class="row">
                        <div class="col col s4 m4 l4">
                            {{-- <img src="{{ asset($user->image) }}" alt="" class="circle responsive-img valign profile-image"> --}}
                        </div>
                        <div class="col col s8 m8 l8">
                            <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">{{ Auth::user()->name }}</a>
                            <p class="user-roal">{{ Auth::user()->email }}</p>
                        </div>
                    </div>
                </li>
                <li id="menu-home"><a href="{{ route('home') }}" class="waves-effect waves-cyan"><i class="mdi-action-home"></i> Home</a></li>
                <li id="menu-profile"><a href="{{ route('profile.index') }}" class="waves-effect waves-cyan"><i class="mdi-social-domain"></i> Company Profile</a></li>
                <li id="menu-jobs"><a href="{{ route('job.index') }}" class="waves-effect waves-cyan"><i class="mdi-action-wallet-travel"></i> Jobs</a></li>
                <li><a href="{{ route('logout') }}" class="waves-effect waves-cyan"><i class="mdi-action-settings-power"></i> Logout</a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">
            <!--breadcrumbs start-->
            <div id="breadcrumbs-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            @yield('breadcrumb')
                        </div>
                    </div>
                </div>
            </div>
            <!--breadcrumbs end-->

            <!--start container-->
            <div class="container">
                <div class="section">
                    @yield('content')
                </div>
            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

    </div>
    <!-- END WRAPPER -->

</div>
<!-- END MAIN -->



<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START FOOTER -->
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <span style="margin-left: 240px">2018 Worklah by <a class="grey-text text-lighten-4" href="#">A-Onegaishimasu</a></span>
            <span class="right"> Theme by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">GeeksLabs</a></span>
        </div>
    </div>
</footer>
<!-- END FOOTER -->


<!-- ================================================
Scripts
================================================ -->

<!-- jQuery Library -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery-1.11.2.min.js') }}"></script>
<!--materialize js-->
<script type="text/javascript" src="{{ asset('js/materialize.js') }}"></script>
<!--scrollbar-->
<script type="text/javascript" src="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>

<script>
    $(function() {
        if ('{{ session('status') }}' !== null) {
            Materialize.toast('{{ session('msg') }}', 3000)
        }
        
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                Materialize.toast('{{ $error }}', 3000)
            @endforeach
        @endif
    });
</script>

@yield('scripts')

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.0
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login | Worklah Company Panel</title>

    <!-- Favicon-->
    <link rel="icon" href="{{ asset('images/logo-worklah.png') }}">

    <!-- CORE CSS-->
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custom CSS-->
    <link href="{{ asset('css/layouts/page-center.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet"
          media="screen,projection">
</head>

<body class="light-blue">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<!-- Login -->
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form action="{{ route('login') }}" method="POST">
            @csrf

            <div class="row">
                <div class="input-field col s12 center">
                    <img src="{{ asset('images/logo-worklah.png') }}" alt="Worklah"
                         class="responsive-img valign" style="height: 100px; width: auto;">
                    <p class="center login-form-text"><span class="blue-text">Worklah</span> Company Panel</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-content-mail prefix"></i>
                    <input id="email" name="email" type="text" required>
                    <label for="email" class="center-align">Email</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock prefix"></i>
                    <input id="password" name="password" type="password" required>
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row margin error-message" hidden>
                <div class="input-field col s12">
                    <p id="error-login-message" class="center red-text"></p>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light blue col s12" type="submit">Login</button>
                </div>
                <div class="input-field col s12">
                    <p class="margin center medium-small sign-up">Don't have company account? <a href="{{ route('register.index') }}">Register</a></p>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- ================================================
Scripts
================================================ -->

<!-- jQuery Library -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery-1.11.2.min.js') }}"></script>
<!--materialize js-->
<script type="text/javascript" src="{{ asset('js/materialize.js') }}"></script>
<!--scrollbar-->
<script type="text/javascript" src="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>

<!--firebase.js - Add Firebase to web-->
<script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>

<script>
    var config = {
        apiKey: "{{ config('services.firebase.api_key') }}",
        authDomain: "{{ config('services.firebase.auth_domain') }}",
        databaseURL: "{{ config('services.firebase.database_url') }}",
        projectId: "{{ config('services.firebase.project_id') }}",
        storageBucket: "{{ config('services.firebase.storage_bucket') }}",
    };
    firebase.initializeApp(config);

    $(function () {
        $('.login-form').on('submit', function (e) {
            e.preventDefault()

            let email = $('input#email').val()
            let password = $('input#password').val()

            let creds = firebase.auth().signInWithEmailAndPassword(email, password)
                .then(async function (response) {
                    let token = await response.user.getIdToken()
                    let xhr = new XMLHttpRequest()
                    let url = '/api/user'

                    xhr.onreadystatechange = function () {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            $('.error-message').attr('hidden', true)

                            Materialize.toast('Login Success, Redirecting...', 3500)

                            let xhrHome = new XMLHttpRequest()
                            let params = 'email=' + email + '&password=' + password + '&_token=' + '{{ csrf_token() }}'

                            xhrHome.open('POST', 'login', true)
                            xhrHome.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'))
                            xhrHome.send(params)
                            window.location = 'home'
                        }
                    }
                    
                    await xhr.open('GET', url, true)
                    xhr.setRequestHeader('Authorization', 'Bearer ' + token)
                    xhr.send()
                })
                .catch(function (error) {
                    let message = error.message
                    let code = error.code
                    console.log(message)

                    if (code.includes('wrong-password') || code.includes('invalid-email')) {
                        message = 'Invalid email/password'
                    } else if (code.includes('network-request-failed')) {
                        message = 'Network error'
                    } else {
                        message += "\nPlease contact the developer for further helps :)"
                    }

                    $('#error-login-message').text(message)
                    $('.error-message').attr('hidden', false)

                    Materialize.toast('Login Failed', 3500)
                })
        })
    })
</script>

</body>

</html>
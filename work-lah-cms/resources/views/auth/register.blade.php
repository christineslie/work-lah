<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.0
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Register | Worklah Company Panel</title>

    <!-- Favicon-->
    <link rel="icon" href="{{ asset('images/logo-worklah.png') }}">

    <!-- CORE CSS-->
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custom CSS-->
    <link href="{{ asset('css/layouts/page-center.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet"
          media="screen,projection">
</head>

<body class="light-blue">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<!-- Login -->
<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form action="{{ route('register.store') }}" method="POST">
            @csrf

            <div class="row">
                <div class="input-field col s12 center">
                    <img src="{{ asset('images/logo-worklah.png') }}" alt="Worklah"
                         class="responsive-img valign" style="height: 100px; width: auto;">
                    <p class="center login-form-text"><span class="blue-text">Worklah</span> Company Panel</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-social-location-city prefix"></i>
                    <input id="name" name="name" type="text" required>
                    <label for="name" class="center-align">Company Name</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-content-mail prefix"></i>
                    <input id="email" name="email" type="text" required>
                    <label for="email" class="center-align">Email</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock prefix"></i>
                    <input id="password" name="password" type="password" required>
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock prefix"></i>
                    <input id="password_confirmation" name="password_confirmation" type="password" required>
                    <label for="password_confirmation">Password again</label>
                </div>
            </div>
            <div class="row margin error-message" hidden>
                <div class="input-field col s12">
                    <p id="error-login-message" class="center red-text"></p>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light blue col s12" type="submit">Register</button>
                </div>
                <div class="input-field col s12">
	            	<p class="margin center medium-small sign-up">Already have an account? <a href="{{ route('login') }}">Login</a></p>
	          	</div>
            </div>
        </form>
    </div>
</div>

<!-- ================================================
Scripts
================================================ -->

<!-- jQuery Library -->
<script type="text/javascript" src="{{ asset('js/plugins/jquery-1.11.2.min.js') }}"></script>
<!--materialize js-->
<script type="text/javascript" src="{{ asset('js/materialize.js') }}"></script>
<!--scrollbar-->
<script type="text/javascript" src="{{ asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>

</body>

</html>
@extends('layouts.app')

@section('title', 'Company Profile')

@section('stylesheets')
<link href="{{ asset('js/plugins/magnific-popup/magnific-popup.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('breadcrumb')
<h5 class="breadcrumbs-title">Company Profile</h5>
<ol class="breadcrumbs">
    <li><a href="{{ route('profile.index') }}">Company Profile</a></li>
</ol>
@endsection

@section('content')
<div class="masonry-gallery-wrapper" style="right: 1.5rem; top: 6rem">
	<div class="popup-gallery">
		<div class="gallary-sizer"></div>
		<div class="gallary-item"><a href="{{ $company['logo'] }}" title="Logo"><img src="{{ $company['logo'] }}" style="height: 150px; width: auto; right: 1.5rem;"></a></div>
	</div>
</div>
<p class="caption">Profile defines professionality.</p>
<div class="divider"></div>

<div class="row">
	<div class="col s12 m12 l12">
        <div class="card-panel">
        	<div class="row">
            	<form id="profile-form" class="col s12" method="POST"  enctype="multipart/form-data">
            		@csrf
            		{{ method_field('PUT') }}
            		<input name='existedImage' value="{{ $company['logo'] }}" hidden>

	            	<div class="row">
	                	<div class="input-field col s12">
	                		<i class="mdi-social-location-city prefix"></i>
	                		<input id="name" name="name" type="text" class="validate" value="{{ $company['companyName'] }}" required>
	                		<label for="name">Company Name</label>
	                	</div>
	              	</div>
	              	<div class="row">
	                	<div class="input-field col s12">
	                  		<i class="mdi-communication-email prefix"></i>
	                  		<input id="email" name="email" type="email" class="validate" value="{{ $company['email'] }}" required>
	                  		<label for="email">Email</label>
	                	</div>
	              	</div>
	              	<div class="row">
	                	<div class="input-field col s12">
	                		<i class="mdi-maps-map prefix"></i>
	                		<input id="location" name="location" type="text" class="validate" value="{{ $company['location'] }}" required>
	                		<label for="location">Location</label>
	                	</div>
	              	</div>
	              	<div class="row">
	                	<div class="input-field col s12">
	                		<i class="mdi-maps-pin-drop prefix"></i>
	                		<input id="longitude" name="longitude" type="text" class="validate" value="{{ $company['coordinates']['longitude'] }}" required>
	                		<label for="longitude">Longitude</label>
	                	</div>
	              	</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="mdi-maps-pin-drop prefix"></i>
							<input id="latitude" name="latitude" type="text" class="validate" value="{{ $company['coordinates']['latitude'] }}" required>
							<label for="latitude">latitude</label>
						</div>
					</div>
	              	<div class="row">
	              		<div class="file-field input-field col s6">
	              			<i class="mdi-image-photo prefix"></i>
	              			<input class="file-path validate" type="text" id="image" />
	              			<input type="text" id="imagePath" name="imagePath" hidden />
	                        <label for="image">New Company Logo</label>
	                        <div class="btn blue waves-effect waves-light">
	                        	<span>Upload new logo</span>
	                        	<input type="file" id="uploadedImage" />
	                        </div>
	                    </div>
		            </div>
	                <div class="row">
	                  	<div class="input-field col s12">
	                    	<button class="btn cyan waves-effect waves-light right" type="button" id="updateBtn" name="action">Update
	                      		<i class="mdi-action-input right"></i>
	                    	</button>
	                  	</div>
	                </div>
	            </div>
	        </form>
	    </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/masonry.pkgd.min.js') }}"></script>
<script src="{{ asset('js/plugins/imagesloaded.pkgd.min.js') }}"></script>    
<script type="text/javascript" src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

<!--firebase.js - Add Firebase to web-->
<script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>

<script type="text/javascript">
	var config = {
        apiKey: "{{ config('services.firebase.api_key') }}",
        authDomain: "{{ config('services.firebase.auth_domain') }}",
        databaseURL: "{{ config('services.firebase.database_url') }}",
        projectId: "{{ config('services.firebase.project_id') }}",
        storageBucket: "{{ config('services.firebase.storage_bucket') }}",
    };
    firebase.initializeApp(config);

	$(function() {
		$('#menu-profile').addClass('active')

		let $containerGallery = $(".masonry-gallery-wrapper");
    	$containerGallery.imagesLoaded(function() {
        	$containerGallery.masonry({
            	itemSelector: '.gallary-item img',
           		columnWidth: '.gallary-sizer',
           		isFitWidth: true
    		});
      	});

      	$('.popup-gallery').magnificPopup({
        	delegate: 'a',
	        type: 'image',
	        closeOnContentClick: true,    
	        fixedContentPos: true,
	        tLoading: 'Loading image #%curr%...',
	        mainClass: 'mfp-img-mobile mfp-no-margins mfp-with-zoom',
	        gallery: {
	        	enabled: true,
	          	navigateByImgClick: true,
	          	preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	        },
	        image: {
	          	verticalFit: true,
	          	tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
	          	titleSrc: function(item) {
	            	return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
	          	},
	        	zoom: {
		          	enabled: true,
		          	duration: 300 // don't foget to change the duration also in CSS
		        }
        	}
      	});

      	$('button#updateBtn').on('click', function () {
	      	let file = $('#uploadedImage').get(0).files[0]

	      	if (file !== undefined) {
		      	let ref = firebase.storage().ref()
				let name = 'jobs/avatar-' + file.name

				Materialize.toast('Uploading your company logo', 2000)
				
				ref.child(name).put(file)
					.then(snapshot => snapshot.ref.getDownloadURL())
				  	.then((url) => {
				  		console.log(url)
				  		$('input#imagePath').attr('value', url)
				  		$('#profile-form').attr('action', "{{ url('profile/' . $uuid) }}").submit()
				  	})
				  	.catch((error) => {
				  		Materialize.toast('Failed uploading logo', 3000)
				  		console.log(error)
				  	});
			} else {
				$('#profile-form').attr('action', "{{ url('profile/' . $uuid) }}").submit()
			}
	  	});
	})
</script>
@endsection
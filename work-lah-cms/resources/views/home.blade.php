@extends('layouts.app')

@section('title', 'Home')

@section('stylesheets')
@endsection

@section('breadcrumb')
<h5 class="breadcrumbs-title">Home</h5>
<ol class="breadcrumbs">
    <li><a href="{{ route('home') }}">Home</a></li>
</ol>
@endsection

@section('content')
<p class="caption">Hello, {{ ucfirst($user->name) }}!</p>
<div class="divider"></div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('#menu-home').addClass('active');
	})
</script>
@endsection
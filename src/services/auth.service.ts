import firebase from 'firebase'
import { Injectable } from '@angular/core'
import { ToastController } from 'ionic-angular'

@Injectable()
export class AuthService {
  constructor(public toastCtrl: ToastController) {}

  signUp(email: string, username: string, password: string) {
    //make sure username isnt used
    let usedUsername: boolean = false
    firebase
      .database()
      .ref('users')
      .orderByChild('username')
      .equalTo(username)
      .once('value', data => {
        if (data.val() != null) {
          console.log('username is taken')
        } else {
          usedUsername = true
        }
      })
    if (!usedUsername) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(data => {
          firebase
            .database()
            .ref('users')
            .child(data.user.uid)
            .set({
              email: data.user.email,
              username: username,
              name: username,
              friends: "",
              reviews: "",
              endorse: "",
              cvimage: "",
              ppimage: "",
              jobstaken: ""
            })
          return true
        })
        .catch(error => {
          this.errorToast(error)
          // alert(error)
          // console.error(error)
        })
    }
  }

  signIn(username: string, password: string) {
    //need to get email from the username
    firebase
      .database()
      .ref('users')
      .orderByChild('username')
      .equalTo(username)
      .once('value', data => {
        if (data.val() != null) {
          let keys = Object.keys(data.val())
          let email = data.val()[keys[0]].email
          firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(user => {
              // alert(user)
            })
            .catch(error => {
              // console.error(error)
            })
        } else {
          let error = 'Error: the username or password is invalid'
          this.errorToast(error)
          //username not found
        }
      })
  }

  signInWithGoogle() {
    //use redirect method
    let provider = new firebase.auth.GoogleAuthProvider()
    firebase.auth().signInWithRedirect(provider)
  }

  forgotPassword(email: string) {
    let auth = firebase.auth()
    auth
      .sendPasswordResetEmail(email)
      .then(() => {
        // Email sent.
      })
      .catch(error => {
        // An error happened.
      })
  }

  logout() {
    firebase.auth().signOut()
  }

  errorToast(error) {
    let toast = this.toastCtrl.create({
      message: error,
      duration: 1500,
      position: 'bottom'
    })

    toast.onDidDismiss(() => {
      console.log('Dismissed toast')
    })

    toast.present()
  }
}

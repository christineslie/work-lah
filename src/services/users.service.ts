import firebase from 'firebase'
import moment from 'moment'
import { Job } from '../classes'
import { Observable } from '../../node_modules/rxjs'
import { AngularFireDatabase } from '../../node_modules/angularfire2/database'
import { Injectable } from '../../node_modules/@angular/core'
import { NavParams } from '../../node_modules/ionic-angular/umd'
@Injectable()
export class UsersService {
  users: Observable<any>
  db: AngularFireDatabase
  constructor(db: AngularFireDatabase) {
    this.db = db
  }

  getUsers() {
    return this.db.object('users').valueChanges()
  }
  getSingleUser(userkey: string) {
    console.log(userkey)
    return this.db.object('users/' + userkey).valueChanges()
  }
  
  getEndorse(userkey: string) {
    return this.db.object('users/' + userkey + '/endorse').valueChanges()
  }
  getReviews(userkey: string) {
    return this.db.object('users/' + userkey + '/reviews').valueChanges()
  }
  getListUsers() {
    console.log(this.db.list('/users').snapshotChanges())
    return this.db.list('/users').snapshotChanges()
  }
}

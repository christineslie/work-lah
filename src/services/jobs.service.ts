import firebase from 'firebase'
import moment from 'moment'
import { Job } from '../classes'
import { Observable } from '../../node_modules/rxjs'
import { AngularFireDatabase } from '../../node_modules/angularfire2/database'
import { Injectable } from '../../node_modules/@angular/core'
import { NavParams } from '../../node_modules/ionic-angular/umd'
@Injectable()
export class JobsService {
  jobs: Observable<any>
  db: AngularFireDatabase
  constructor(db: AngularFireDatabase) {
    this.db = db
    // this.jobs = db.object('jobs').valueChanges()
  }

  getJobs() {
    return this.db.object('jobs').valueChanges()
  }
  getSingleJob(jobKey: string) {
    console.log(jobKey)
    return this.db.object('jobs/' + jobKey).valueChanges()
  }
  getJobsByCategory(category: string) {
    console.log(category)
    return this.db
      .list('/jobs', ref => ref.orderByChild('category').equalTo(category))
      .snapshotChanges()
  }
  getListJobs() {
    return this.db.list('/jobs').snapshotChanges()
  }
  // getJobsByKeyword(keyword: string) {
  //   // this.startWith.next(queryText)
  //   // this.endWith.next(queryText + '\uf8ff')
  //   return this.db
  //     .list('/jobs', ref =>
  //       ref
  //         .orderByChild('role')
  //         .startAt(keyword)
  //         .endAt(keyword + '\uf8ff')
  //     )
  //     .snapshotChanges()
  // }
}

import firebase from 'firebase'
import moment from 'moment'
import { Job } from '../classes'
import { Observable } from '../../node_modules/rxjs'
import { AngularFireDatabase } from '../../node_modules/angularfire2/database'
import { Injectable } from '../../node_modules/@angular/core'
import { NavParams } from '../../node_modules/ionic-angular/umd'
@Injectable()
export class CompanyService {
  db: AngularFireDatabase
  constructor(db: AngularFireDatabase) {
    this.db = db
  }

  getCompanies() {
    return this.db.object('companies').valueChanges()
  }
  getSingleCompany(companyKey: string) {
    console.log(companyKey)
    return this.db.object('companies/' + companyKey).valueChanges()
  }
  // getJobsByKeyword(keyword: string) {
  //   // this.startWith.next(queryText)
  //   // this.endWith.next(queryText + '\uf8ff')
  //   return this.db
  //     .list('/jobs', ref =>
  //       ref
  //         .orderByChild('role')
  //         .startAt(keyword)
  //         .endAt(keyword + '\uf8ff')
  //     )
  //     .snapshotChanges()
  // }
}

import firebase from 'firebase'
import moment from 'moment'
import { Job, JobApply } from '../classes'
import { Observable } from '../../node_modules/rxjs'
import { AngularFireDatabase } from '../../node_modules/angularfire2/database'
import { Injectable } from '../../node_modules/@angular/core'
import { NavParams } from '../../node_modules/ionic-angular/umd'
@Injectable()
export class JobApplyService {
  db: AngularFireDatabase
  constructor(db: AngularFireDatabase) {
    this.db = db
  }

  getJobApply() {
    return this.db.object('jobApply').valueChanges()
  }
  getSingleJobApply(jobApplyKey: string) {
    return this.db.object('jobApply/' + jobApplyKey).valueChanges()
  }
  getJobApplyByUser() {
    return this.db
      .list('/jobApply', ref => ref.orderByChild('userID').equalTo(firebase.auth().currentUser.uid))
      .snapshotChanges()
  }
  addJobApply(jobApply: JobApply) {
    console.log('A')
    console.log(jobApply)
    this.db
      .list('jobApply')
      .push(jobApply)
      .then(() => {
        return Promise.resolve()
      })
  }
  doTerminate(jobApplyKey: string, jobID: string) {
    this.db
      .list('jobApply')
      .update(jobApplyKey, {
        'status': 'terminated',
        'jobID': jobID,
        'userID': firebase.auth().currentUser.uid
      })
      .then(() => {
        return Promise.resolve()
      })
  }
  // getJobsByKeyword(keyword: string) {
  //   // this.endWith.next(queryText + '\uf8ff')
  //   return this.db  //   // this.startWith.next(queryText)

  //     .list('/jobs', ref =>
  //       ref
  //         .orderByChild('role')
  //         .startAt(keyword)
  //         .endAt(keyword + '\uf8ff')
  //     )
  //     .snapshotChanges()
  // }
}

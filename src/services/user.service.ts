import firebase from 'firebase'
import moment from 'moment'

export class UserService {
  user: {
    email: string, 
    name: string, 
    username: string, 
    friends: Array<string>,
    reviews: any,
    endorse: any,
    cvimage: string,
    ppimage: string,
    jobstaken: any
  }
  fetchUser(): any {
    return new Promise(resolve => {
        firebase
            .database()
            .ref('/users/' + firebase.auth().currentUser.uid + '/')
            .on('value', data => {
                this.user = data.val()
                console.log(this.user)
                resolve()
        })
    })
  }
  getUser() {
    return this.user
  }
  setUserName(nama: string){
    firebase.database().ref('/users/'+ firebase.auth().currentUser.uid).child('name').set(nama)       ///modify data
  }
  setUserReview(i: any){
    firebase.database().ref('/users/'+ firebase.auth().currentUser.uid + "/reviews/").child('review'+i).set({company: "asd", content: "giid"})       ///modify data
  }
  setCVFile(fileUrl: string){
    firebase.database().ref('/users/'+ firebase.auth().currentUser.uid).child('cvimage').set(fileUrl)       ///modify data
  }
  setImage(imgUrl: string){
    firebase.database().ref('/users/'+ firebase.auth().currentUser.uid).child('ppimage').set(imgUrl)       ///modify data
  }

  addFriend(i : number, friendkey: string, j: number){
    firebase.database().ref('/users/'+ firebase.auth().currentUser.uid + "/friends/").child('friend'+i).set(friendkey)     ///add child data
    firebase.database().ref('/users/'+ friendkey + "/friends/").child('friend'+j).set(firebase.auth().currentUser.uid) 
  }

  addEndorse(targetkey: string, content: string){
    firebase.database().ref('/users/'+ targetkey+'/endorse/').child(firebase.auth().currentUser.uid).set(
      {
        name: this.user.name, 
        ppimage: this.user.ppimage, 
        endorsment: content
      }
    )
  }

}

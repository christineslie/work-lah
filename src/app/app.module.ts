import { BrowserModule } from '@angular/platform-browser'
import { ErrorHandler, NgModule } from '@angular/core'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular'

import { MyApp } from './app.component'
import { HomePage } from '../pages/home/home'
import { DetailsPartTimePage } from '../pages/details-part-time/details-part-time'
import { ProfilePage } from '../pages/profile/profile'
import { ConnectionProfilePage } from '../pages/connection-profile/connection-profile'

import { FileChooser } from '@ionic-native/file-chooser'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path';
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'
import { SearchTabsPage } from '../pages/search-tabs/search-tabs'
import { JobSearchPage } from '../pages/job-search/job-search'
import { PeopleSearchPage } from '../pages/people-search/people-search'
import { ApplySuccessPage } from '../pages/apply-success/apply-success'
import { MyJobTabsPage } from '../pages/my-job-tabs/my-job-tabs'
import { MyPartTimePage } from '../pages/my-part-time/my-part-time'
import { MyVolunteerPage } from '../pages/my-volunteer/my-volunteer'
import { MyFreelancePage } from '../pages/my-freelance/my-freelance'
import { ApplyOffersPage } from '../pages/apply-offers/apply-offers'
import { DetailsFreelancePage } from '../pages/details-freelance/details-freelance'
import { DetailsVolunteerPage } from '../pages/details-volunteer/details-volunteer'
import { OfferReceivePage } from '../pages/offer-receive/offer-receive'
import { OfferAcceptedPage } from '../pages/offer-accepted/offer-accepted'
import { AuthPage } from '../pages/auth/auth'
import { SchedulePage } from '../pages/schedule/schedule'
import { NoJobPage } from '../pages/no-job/no-job'

import { AuthService } from '../services/auth.service'
import { JobsService } from '../services/jobs.service'
import { AngularFireModule } from 'angularfire2'
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFireStorageModule } from 'angularfire2/storage'
import { UsersService } from '../services/users.service'
import { Geolocation } from '../../node_modules/@ionic-native/geolocation'
import { CompanyService } from '../services/company.service'
import { JobApplyService } from '../services/jobApply.service'
import { UserService } from '../services/user.service'
import { ToastService } from '../services/toast.service'
import { PopoverPage } from '../pages/popover/popover';

@NgModule({
  declarations: [
    MyApp,
    AuthPage,
    HomePage,
    DetailsPartTimePage,
    DetailsFreelancePage,
    DetailsVolunteerPage,
    ProfilePage,
    ConnectionProfilePage,
    SearchTabsPage,
    JobSearchPage,
    PeopleSearchPage,
    ApplySuccessPage,
    MyJobTabsPage,
    MyPartTimePage,
    MyFreelancePage,
    MyVolunteerPage,
    NoJobPage,
    ApplyOffersPage,
    OfferReceivePage,
    OfferAcceptedPage,
    SchedulePage,
    PopoverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    }),
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyDfOyEUo30ZbThNVhNP680wpi5EeTXAMqA',
      authDomain: 'work-lah.firebaseapp.com',
      databaseURL: 'https://work-lah.firebaseio.com',
      storageBucket: 'work-lah.appspot.com'
    }),
    AngularFireDatabaseModule,
    AngularFireStorageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AuthPage,
    HomePage,
    DetailsPartTimePage,
    DetailsFreelancePage,
    DetailsVolunteerPage,
    ProfilePage,
    ConnectionProfilePage,
    SearchTabsPage,
    JobSearchPage,
    PeopleSearchPage,
    ApplySuccessPage,
    MyJobTabsPage,
    MyPartTimePage,
    MyFreelancePage,
    MyVolunteerPage,
    NoJobPage,
    ApplyOffersPage,
    OfferReceivePage,
    OfferAcceptedPage,
    SchedulePage,
    PopoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    JobsService,
    UsersService,
    UserService,
    CompanyService,
    JobApplyService,
    ToastService,
    Geolocation,
    FileChooser,
    File,
    FilePath,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}

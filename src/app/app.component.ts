import { Component, ViewChild } from '@angular/core'
import { Nav, Platform, AlertController, ToastController } from 'ionic-angular'
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'

import { HomePage } from '../pages/home/home'
import { ProfilePage } from '../pages/profile/profile'
import { MyJobTabsPage } from '../pages/my-job-tabs/my-job-tabs'
import { AuthPage } from '../pages/auth/auth'
import { SchedulePage } from '../pages/schedule/schedule'

import firebase from 'firebase'
import { AuthService } from '../services/auth.service'
import { UserService } from '../services/user.service'

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav

  // rootPage: any = AuthPage

  pages: Array<{ title: string; component: any; icon: string }>

  isSignIn: boolean = false

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public authService: AuthService,
    public userService: UserService,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    this.initializeApp()

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'ios-home-outline' },
      { title: 'My Profile', component: ProfilePage, icon: 'ios-star-outline' },
      { title: 'My Job', component: MyJobTabsPage, icon: 'ios-briefcase-outline' },
      { title: 'My Schedule', component: SchedulePage, icon: 'ios-calendar-outline' },
      { title: 'Logout', component: null, icon: 'ios-log-out-outline' }
    ]
  }

  initializeApp() {
    this.platform.ready().then(() => {
      firebase.initializeApp({
        apiKey: 'AIzaSyDfOyEUo30ZbThNVhNP680wpi5EeTXAMqA',
        authDomain: 'work-lah.firebaseapp.com',
        databaseURL: 'https://work-lah.firebaseio.com',
        projectId: 'work-lah',
        storageBucket: 'work-lah.appspot.com',
        messagingSenderId: '1049103273441'
      })

      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          //do something here if the user is logged in
          this.isSignIn = true
          this.userService.fetchUser()
          this.nav.setRoot(HomePage)
          // WELCOME TOAST
          this.presentToast()
        } else {
          this.isSignIn = false
          this.nav.setRoot(AuthPage)
          //do something here if the user is not logged in
        }
      })

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault()
      this.statusBar.overlaysWebView(false)
      this.splashScreen.hide()
    })
  }

  openPage(page) {
    if (page.component) {
      this.nav.setRoot(page.component)
    } else {
      let alert = this.alertCtrl.create({
        title: 'Sure to logout?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: input => {
              console.log('Cancel clicked')
            }
          },
          {
            text: 'OK',
            handler: input => {
              this.nav.setRoot(AuthPage)
            }
          }
        ]
      })
      alert.present()
    }
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.title == 'Logout') this.authService.logout()
    this.nav.setRoot(page.component)
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Welcome Sandro!',
      duration: 1500,
      position: 'bottom'
    })

    toast.onDidDismiss(() => {
      console.log('Dismissed toast')
    })

    toast.present()
  }
}

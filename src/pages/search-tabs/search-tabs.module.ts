import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchTabsPage } from './search-tabs';

@NgModule({
  declarations: [
    SearchTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchTabsPage),
  ],
})
export class SearchTabsPageModule {}

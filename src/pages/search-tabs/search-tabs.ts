import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular'
import { JobSearchPage } from '../job-search/job-search'
import { PeopleSearchPage } from '../people-search/people-search'
import { Job } from '../../classes'

@IonicPage()
@Component({
  selector: 'page-search-tabs',
  templateUrl: 'search-tabs.html'
})
export class SearchTabsPage {
  jobSearch = JobSearchPage
  peopleSearch = PeopleSearchPage
  keyword: string = ''
  jobListByCategory: Array<Job> = null
  data = {
    viewCtrl: this.viewCtrl,
    byCategory: this.navParams.get('byCategory') || '',
    keyword: this.navParams.get('keyword') || '',
    coordinates: this.navParams.get('coordinates') || null
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.keyword = this.navParams.get('keyword')
    console.log(this.keyword)

    // this.jobsService.getJobsByCategory(this.category).subscribe((value: Array<Job>) => {
    //   console.log(value)
    //   this.jobListByCategory = value
    //   console.log(this.jobListByCategory)
    // })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchTabsPage')
  }
}

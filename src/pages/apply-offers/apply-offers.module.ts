import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplyOffersPage } from './apply-offers';

@NgModule({
  declarations: [
    ApplyOffersPage,
  ],
  imports: [
    IonicPageModule.forChild(ApplyOffersPage),
  ],
})
export class ApplyOffersPageModule {}

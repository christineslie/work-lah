import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { DetailsPartTimePage } from '../details-part-time/details-part-time'
import { DetailsFreelancePage } from '../details-freelance/details-freelance'
import { DetailsVolunteerPage } from '../details-volunteer/details-volunteer'
import { OfferReceivePage } from '../offer-receive/offer-receive'

/**
 * Generated class for the ApplyOffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-apply-offers',
  templateUrl: 'apply-offers.html'
})
export class ApplyOffersPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApplyOffersPage')
  }

  toDetails(jobType) {
    switch (jobType) {
      case 'part-time':
        this.navCtrl.push(DetailsPartTimePage)
        break
      case 'freelance':
        this.navCtrl.push(DetailsFreelancePage)
        break
      case 'volunteer':
        this.navCtrl.push(DetailsVolunteerPage)
        break
    }
  }

  toOffer() {
    this.navCtrl.push(OfferReceivePage)
  }
}

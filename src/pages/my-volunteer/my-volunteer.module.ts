import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyVolunteerPage } from './my-volunteer';

@NgModule({
  declarations: [
    MyVolunteerPage,
  ],
  imports: [
    IonicPageModule.forChild(MyVolunteerPage),
  ],
})
export class MyVolunteerPageModule {}

import { Component } from '@angular/core'
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ToastController
} from 'ionic-angular'
import { Job, Company } from '../../classes';
import { JobApplyService } from '../../services/jobApply.service';
import { JobsService } from '../../services/jobs.service';
import { CompanyService } from '../../services/company.service';

/**
 * Generated class for the MyVolunteerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-volunteer',
  templateUrl: 'my-volunteer.html'
})
export class MyVolunteerPage {
  volunteer: Job
  jobApplyKey: string
  jobID: string
  companyID: string
  company: Company

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private jobApplyService: JobApplyService,
    private jobService: JobsService,
    private companyService: CompanyService
  ) {
    this.jobApplyService
      .getJobApplyByUser()
      .subscribe((value: any) => {
        // this.job = ''
        const jobApplySnapshots = value
        jobApplySnapshots.forEach(jobApplySnapshot => {
          const jobValue = jobApplySnapshot.payload.val()
          if (jobValue.status == 'accepted') {
            console.log(jobValue)
            this.getVolunteerJob(jobValue.jobID, jobApplySnapshot.key)
          }
        })
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyVolunteerPage')
  }

  getVolunteerJob(jobID: string, jobApplyKey: string) {
    this.jobService
      .getSingleJob(jobID)
      .subscribe((value: Job) => {
        this.volunteer = null
        if (value.type == 'volunteer') {
          this.jobID = jobID
          this.jobApplyKey = jobApplyKey
          this.volunteer = value
          this.companyID = value.companyID
          console.log(this.volunteer)
          this.getCompanyDetail()
        }
      })
  }

  getCompanyDetail() {
    this.companyService
      .getSingleCompany(this.companyID)
      .subscribe((value: Company) => {
        this.company = value
        console.log(this.company)
      })
  }

  onTerminate(jobApplyKey: string, jobID: string) {
    let alert = this.alertCtrl.create({
      title: 'Sure to stop this Volunteer?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: input => {
            console.log('Cancel clicked')
          }
        },
        {
          text: 'OK',
          handler: input => {
            console.log(this.jobApplyKey)
            this.jobApplyService
              .doTerminate(jobApplyKey, jobID)
            this.terminateSuccessToast()
          }
        }
      ]
    })
    alert.present()
  }

  terminateSuccessToast() {
    let toast = this.toastCtrl.create({
      message: 'Termination email has been sent to company',
      duration: 1500,
      position: 'bottom'
    })

    toast.onDidDismiss(() => {
      console.log('Dismissed termination toast')
    })

    toast.present()
  }
}

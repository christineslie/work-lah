import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { MyFreelancePage } from '../my-freelance/my-freelance'
import { MyVolunteerPage } from '../my-volunteer/my-volunteer'
import { MyPartTimePage } from '../my-part-time/my-part-time'
import { ApplyOffersPage } from '../apply-offers/apply-offers'

/**
 * Generated class for the MyJobTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-job-tabs',
  templateUrl: 'my-job-tabs.html'
})
export class MyJobTabsPage {
  partTime = MyPartTimePage
  freelance = MyFreelancePage
  volunteer = MyVolunteerPage
  applyOffer = ApplyOffersPage
  // noJob = NoJobPage

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyJobTabsPage')
  }
}

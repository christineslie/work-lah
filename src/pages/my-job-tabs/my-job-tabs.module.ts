import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyJobTabsPage } from './my-job-tabs';

@NgModule({
  declarations: [
    MyJobTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyJobTabsPage),
  ],
})
export class MyJobTabsPageModule {}

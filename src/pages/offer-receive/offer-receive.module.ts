import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferReceivePage } from './offer-receive';

@NgModule({
  declarations: [
    OfferReceivePage,
  ],
  imports: [
    IonicPageModule.forChild(OfferReceivePage),
  ],
})
export class OfferReceivePageModule {}

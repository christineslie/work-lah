import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { OfferAcceptedPage } from '../offer-accepted/offer-accepted'

/**
 * Generated class for the OfferReceivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offer-receive',
  templateUrl: 'offer-receive.html'
})
export class OfferReceivePage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferReceivePage')
  }

  onAcceptOffer() {
    this.navCtrl.push(OfferAcceptedPage)
  }
}

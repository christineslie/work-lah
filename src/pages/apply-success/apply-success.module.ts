import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplySuccessPage } from './apply-success';

@NgModule({
  declarations: [
    ApplySuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(ApplySuccessPage),
  ],
})
export class ApplySuccessPageModule {}

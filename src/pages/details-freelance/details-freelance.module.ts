import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailsFreelancePage } from './details-freelance';

@NgModule({
  declarations: [
    DetailsFreelancePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailsFreelancePage),
  ],
})
export class DetailsFreelancePageModule {}

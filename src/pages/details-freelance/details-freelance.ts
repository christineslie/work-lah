import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { ApplySuccessPage } from '../apply-success/apply-success'
import { JobsService } from '../../services/jobs.service'
import { Job, Company, JobApply } from '../../classes'
import moment from 'moment'
import { CompanyService } from '../../services/company.service'
import firebase from 'firebase'
import { JobApplyService } from '../../services/jobApply.service'

/**
 * Generated class for the DetailsFreelancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details-freelance',
  templateUrl: 'details-freelance.html'
})
export class DetailsFreelancePage {
  loaded: boolean
  freelanceJobs: Job = null
  company: Company = null
  jobKey: string
  isClicked: boolean
  isApplied: boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private jobsService: JobsService,
    private companyService: CompanyService,
    private jobApplyService: JobApplyService
  ) {
    this.jobKey = navParams.get('jobKey')
    this.isApplied = navParams.get('isApplied')
    this.loaded = false
    this.jobsService.getSingleJob(this.jobKey).subscribe((value: Job) => {
      this.freelanceJobs = value
      this.companyService
        .getSingleCompany(this.freelanceJobs.companyID)
        .subscribe((value: Company) => {
          this.company = value
          console.log(this.company)
        })
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsFreelancePage')
  }
  showDate(date) {
    return moment(date).format('LL')
  }
  showWeeklyDeadline(day) {
    const firstLetter = day.charAt(0).toUpperCase()
    const withoutFirstLetter = day.slice(1)
    return firstLetter + withoutFirstLetter
  }
  async onSuccessApply() {
    this.isClicked = true
    await this.jobApplyService.addJobApply(
      new JobApply(firebase.auth().currentUser.uid, 'applied', this.jobKey)
    )
    this.navCtrl.push(ApplySuccessPage)
  }
}

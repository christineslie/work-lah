import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyFreelancePage } from './my-freelance';

@NgModule({
  declarations: [
    MyFreelancePage,
  ],
  imports: [
    IonicPageModule.forChild(MyFreelancePage),
  ],
})
export class MyFreelancePageModule {}

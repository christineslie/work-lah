import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyPartTimePage } from './my-part-time';

@NgModule({
  declarations: [
    MyPartTimePage,
  ],
  imports: [
    IonicPageModule.forChild(MyPartTimePage),
  ],
})
export class MyPartTimePageModule {}

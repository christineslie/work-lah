import { Component } from '@angular/core'
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ToastController
} from 'ionic-angular'
import { Job, JobApply, Company } from '../../classes'
import { JobApplyService } from '../../services/jobApply.service'
import { JobsService } from '../../services/jobs.service'
import { CompanyService } from '../../services/company.service'
import moment from 'moment'

/**
 * Generated class for the MyPartTimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-part-time',
  templateUrl: 'my-part-time.html'
})
export class MyPartTimePage {
  partTime: Job
  jobApplyKey: string
  jobID: string
  companyID: string
  company: Company
  workDays: string[] = []
  workDates: number[] = []

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private jobApplyService: JobApplyService,
    private jobService: JobsService,
    private companyService: CompanyService
  ) {
    this.jobApplyService.getJobApplyByUser().subscribe((value: any) => {
      // this.job = ''
      const jobApplySnapshots = value
      jobApplySnapshots.forEach(jobApplySnapshot => {
        const jobValue = jobApplySnapshot.payload.val()
        if (jobValue.status == 'accepted') {
          console.log(jobValue)
          this.getPartTimeJob(jobValue.jobID, jobApplySnapshot.key)
        }
      })
    })
  }

  getPartTimeJob(jobID: string, jobApplyKey: string) {
    this.jobService.getSingleJob(jobID).subscribe((value: Job) => {
      this.partTime = null
      if (value.type == 'part-time') {
        this.jobID = jobID
        this.jobApplyKey = jobApplyKey
        this.partTime = value
        this.companyID = value.companyID
        console.log(this.partTime)
        this.getSchedules()
        this.getCompanyDetail()
      }
    })
  }

  getCompanyDetail() {
    this.companyService.getSingleCompany(this.companyID).subscribe((value: Company) => {
      this.company = value
      console.log(this.company)
    })
  }

  getSchedules() {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

    let currentDate = moment().format('DD')
    let currentDay = new Date().getDay()
    let schedules = this.partTime.schedules.shiftDay
    console.log(schedules)

    for (let i = 0; i < 7; i++) {
      for (let schedule in schedules) {
        if (i >= currentDay) {
          if (days[i].toLowerCase() == schedule) {
            this.workDays.push(days[i])
            this.workDates.push(parseInt(currentDate) + i)
          }
        }
      }
    }
    console.log(this.workDays)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyPartTimePage')
  }

  onTerminate(jobApplyKey: string, jobID: string) {
    let alert = this.alertCtrl.create({
      title: 'Sure to stop this Part-Time?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: input => {
            console.log('Cancel clicked')
          }
        },
        {
          text: 'OK',
          handler: input => {
            console.log(this.jobApplyKey)
            this.jobApplyService.doTerminate(jobApplyKey, jobID)
            this.terminateSuccessToast()
          }
        }
      ]
    })
    alert.present()
  }

  terminateSuccessToast() {
    let toast = this.toastCtrl.create({
      message: 'Termination email has been sent to company',
      duration: 1500,
      position: 'bottom'
    })

    toast.onDidDismiss(() => {
      console.log('Dismissed termination toast')
    })

    toast.present()
  }
}

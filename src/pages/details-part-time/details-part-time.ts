import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { ApplySuccessPage } from '../apply-success/apply-success'
import { JobsService } from '../../services/jobs.service'
import { Job, Company, JobApply } from '../../classes'
import moment from 'moment'
import { CompanyService } from '../../services/company.service'
import { JobApplyService } from '../../services/jobApply.service'
import firebase from 'firebase'
/**
 * Generated class for the DetailsPartTimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details-part-time',
  templateUrl: 'details-part-time.html'
})
export class DetailsPartTimePage {
  partTimeJobs: Job = null
  company: Company = null
  loaded: boolean
  jobKey: string
  isApplied: boolean
  isClicked: boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private jobsService: JobsService,
    private companyService: CompanyService,
    private jobApplyService: JobApplyService
  ) {
    this.jobKey = navParams.get('jobKey')
    this.isApplied = navParams.get('isApplied')
    this.loaded = false
    this.isClicked = false
    this.jobsService.getSingleJob(this.jobKey).subscribe((value: Job) => {
      this.partTimeJobs = value
      this.companyService
        .getSingleCompany(this.partTimeJobs.companyID)
        .subscribe((value: Company) => {
          this.company = value
          console.log(this.company)
        })
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPartTimePage')
  }
  showDate(date) {
    return moment(date).format('LL')
  }
  async onSuccessApply() {
    this.isClicked = true
    await this.jobApplyService.addJobApply(
      new JobApply(firebase.auth().currentUser.uid, 'applied', this.jobKey)
    )
    this.navCtrl.push(ApplySuccessPage)
  }
}

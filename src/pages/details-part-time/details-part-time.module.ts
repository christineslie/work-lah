import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { DetailsPartTimePage } from './details-part-time'

@NgModule({
  declarations: [DetailsPartTimePage],
  imports: [IonicPageModule.forChild(DetailsPartTimePage)]
})
export class DetailsPartTimePageModule {}

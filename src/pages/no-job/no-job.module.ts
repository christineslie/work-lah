import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoJobPage } from './no-job';

@NgModule({
  declarations: [
    NoJobPage,
  ],
  imports: [
    IonicPageModule.forChild(NoJobPage),
  ],
})
export class NoJobPageModule {}

import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { JobApplyService } from '../../services/jobApply.service'
import firebase from 'firebase'
import { JobApply, Job, Company } from '../../classes'
import { JobsService } from '../../services/jobs.service'
import { CompanyService } from '../../services/company.service'
import moment from 'moment'
/**
 * Generated class for the SchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})
export class SchedulePage {
  date: any
  daysInThisMonth: any
  daysInLastMonth: any
  daysInNextMonth: any
  monthNames: string[]
  currentMonth: any
  currentYear: any
  currentDate: any
  eventList: any[]
  selectedEvent: any

  dateInMonthList: any[]

  ptDaysIndex: number[] = []
  fDeadline: number
  vDates: Date[] = []
  selectedDate: any
  dayNames: string[]

  jobApply: Array<JobApply>
  jobs: Array<Job> = []
  jobKeys: Array<string> = []
  companies: Array<Company> = []
  loading: boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private jobApplyService: JobApplyService,
    private jobsService: JobsService,
    private companyService: CompanyService
  ) {}

  ionViewWillEnter() {
    this.loading = true
    this.date = new Date()
    this.monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]
    this.dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    this.getDaysOfMonth(this.date.getMonth())

    // // MOCK DATA
    // this.ptDaysIndex = [1, 2, 5]
    // this.fDeadline = new Date().setHours(0, 0, 0, 0)
    // this.vDates = [new Date(2018, 11, 16), new Date(2018, 11, 17), new Date(2018, 11, 18)]
    // this.selectedDate = {
    //   date: new Date().getDate(),
    //   fullDate: '2018/12/11',
    //   dayName: this.dayNames[new Date().getDay()]
    // }
  }
  ionViewDidLoad() {
    this.companyService.getCompanies().subscribe((value: Array<Company>) => {
      this.companies = value
    })
    this.jobsService.getJobs().subscribe((value: Array<Job>) => {
      this.jobs = value
      this.jobKeys = Object.keys(this.jobs)
    })
    this.jobApplyService
      .getJobApplyByUser()
      .subscribe((value: any) => {
        this.jobApply = []
        const jobApplySnapshots = value
        jobApplySnapshots.forEach(jobApplySnapshot => {
          const jobValue = jobApplySnapshot.payload.val()
          if (jobValue.status == 'accepted') {
            if (this.jobs[jobValue.jobID].type == 'part-time') {
              if (this.jobs[jobValue.jobID].schedules.shiftDay['monday']) this.ptDaysIndex.push(1)
              if (this.jobs[jobValue.jobID].schedules.shiftDay['tuesday']) this.ptDaysIndex.push(2)
              if (this.jobs[jobValue.jobID].schedules.shiftDay['wednesday'])
                this.ptDaysIndex.push(3)
              if (this.jobs[jobValue.jobID].schedules.shiftDay['thursday']) this.ptDaysIndex.push(4)
              if (this.jobs[jobValue.jobID].schedules.shiftDay['friday']) this.ptDaysIndex.push(5)
              if (this.jobs[jobValue.jobID].schedules.shiftDay['saturday']) this.ptDaysIndex.push(6)
              if (this.jobs[jobValue.jobID].schedules.shiftDay['sunday']) this.ptDaysIndex.push(0)
            } else if (this.jobs[jobValue.jobID].type == 'freelance') {
              this.fDeadline = moment(this.jobs[jobValue.jobID].schedules['submitDeadline'])
                .toDate()
                .setHours(0, 0, 0, 0)
            } else if (this.jobs[jobValue.jobID].type == 'volunteer') {
              const startDate = moment(
                this.jobs[jobValue.jobID].schedules['volunteerDate'].dateStart
              ).toDate()
              const endDate = moment(
                this.jobs[jobValue.jobID].schedules['volunteerDate'].dateEnd
              ).toDate()

              for (let d = startDate; d <= endDate; d.setDate(d.getDate() + 1)) {
                this.vDates.push(new Date(d))
              }
            }
          }
        })
      })
  }

  async getDaysOfMonth(selectedMonth) {
    this.daysInThisMonth = []
    this.daysInLastMonth = []
    this.daysInNextMonth = []
    this.dateInMonthList = []
    this.currentMonth = this.monthNames[this.date.getMonth()]
    this.currentYear = this.date.getFullYear()

    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate()
    } else {
      this.currentDate = 999
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), selectedMonth, 1).getDay()
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate()
    for (let i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i)
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate()
    for (let i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push(i + 1)
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay()
    var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate()
    for (let i = 0; i < 6 - lastDayThisMonth; i++) {
      this.daysInNextMonth.push(i + 1)
    }
    var totalDays =
      this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length
    if (totalDays < 36) {
      for (let i = 7 - lastDayThisMonth; i < 7 - lastDayThisMonth + 7; i++) {
        this.daysInNextMonth.push(i)
      }
    }

    // Load every Date(s) in month
    await this.daysInThisMonth.forEach(day => {
      this.dateInMonthList.push(new Date(this.date.getFullYear(), this.date.getMonth(), day))
    })

    if (this.currentDate !== 999) {
      let tempDate = this.dateInMonthList[this.currentDate - 1]
      console.log('CURR', this.currentDate)
      this.selectedDate = {
        date: new Date().getDate(),
        fullDate:
          tempDate.getFullYear() + '/' + (tempDate.getMonth() + 1) + '/' + tempDate.getDate(),
        dayName: this.dayNames[new Date().getDay()]
      }
    }

    this.loadEventThisMonth()
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0)
    this.getDaysOfMonth(this.date.getMonth())
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0)
    this.getDaysOfMonth(this.date.getMonth())
  }

  async loadEventThisMonth() {
    this.eventList = []

    // if (this.currentDate) {
    //   console.log(this.currentDate)
    //   this.selectedDate = {
    //     date: new Date().getDate(),
    //     dayName: this.dayNames[new Date().getDay()]
    //   }
    // }

    await this.dateInMonthList.forEach(date => {
      this.ptDaysIndex.forEach(ptDay => {
        if (ptDay === date.getDay()) {
          this.eventList[date.getDate()] = { ...this.eventList[date.getDate()], hasPT: true }
        }
      })
      this.vDates.forEach(vDate => {
        if (vDate.setHours(0, 0, 0, 0) == date.setHours(0, 0, 0, 0)) {
          this.eventList[date.getDate()] = { ...this.eventList[date.getDate()], hasV: true }
        }
      })
      if (this.fDeadline === date.getTime())
        this.eventList[date.getDate()] = { ...this.eventList[date.getDate()], hasF: true }
    })
  }

  hasPT(day) {
    if (this.eventList[day]) {
      return this.eventList[day].hasPT
    }
    return false
  }

  hasF(day) {
    if (this.eventList[day]) return this.eventList[day].hasF
    return false
  }

  hasV(day) {
    if (this.eventList[day]) return this.eventList[day].hasV
    return false
  }

  isSunday(day) {
    let sundayList = this.dateInMonthList
      .filter(date => {
        if (date.getDay() === 0) return date.getDate()
      })
      .map(date => date.getDate())
    if (sundayList.indexOf(day) > -1) return true
    return false
  }

  isSelected(day) {
    if (this.selectedDate.date === day) return true
  }

  selectDate(date) {
    let tempDate = this.dateInMonthList[date - 1]
    let fullDate =
      tempDate.getFullYear() + '/' + (tempDate.getMonth() + 1) + '/' + tempDate.getDate()
    let dayName = this.dayNames[tempDate.getDay()]
    this.selectedDate = {
      date,
      fullDate,
      dayName
    }
  }

  // selectDate(day) {
  //   this.isSelected = false
  //   this.selectedEvent = new Array()
  //   var thisDate1 =
  //     this.date.getFullYear() + '-' + (this.date.getMonth() + 1) + '-' + day + ' 00:00:00'
  //   var thisDate2 =
  //     this.date.getFullYear() + '-' + (this.date.getMonth() + 1) + '-' + day + ' 23:59:59'
  //   this.eventList.forEach(event => {
  //     if (
  //       (event.startDate >= thisDate1 && event.startDate <= thisDate2) ||
  //       (event.endDate >= thisDate1 && event.endDate <= thisDate2)
  //     ) {
  //       this.isSelected = true
  //       this.selectedEvent.push(event)
  //     }
  //   })
  // }
}

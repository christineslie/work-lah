import { Component } from '@angular/core'
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  AlertController,
  PopoverController
} from 'ionic-angular'
import { ConnectionProfilePage } from '../connection-profile/connection-profile'
//import { Observable } from 'rxjs';

import { FileChooser } from '@ionic-native/file-chooser'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path'
import firebase from 'firebase'
import { AngularFireAuth } from 'angularfire2/auth'
import { UserService } from '../../services/user.service'
import { User, Friend, Endorse, Review } from '../../classes'
import { UsersService } from '../../services/users.service'
import { PopoverPage } from '../popover/popover'
import { UrlResolver } from '@angular/compiler'

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  section: string
  user: {
    email: string
    name: string
    username: string
    friends: Array<string>
    reviews: Array<string>
    endorse: Array<string>
    cvimage: string
    ppimage: string
    jobstaken: any
  }

  urlcvfile: any
  username: string
  newUrl: string
  ppimage: string
  star: number = 0
  starhalf: boolean = false
  starfull: Array<number> = this.starfull || []
  starblank: Array<number> = this.starblank || []
  starcounter: number = 0

  users: Array<User> = null
  usersnum: number
  usersKey: Array<string> = []

  friends: Array<string>
  myfriendsdata: Array<Friend>
  friendsnum: number

  Review: Array<Review>
  Reviewlist: Array<Review>
  Reviewnum: number
  Reviewkey: Array<string>

  Endorse: Array<Endorse>
  Endorselist: Array<Endorse>
  Endorsenum: number
  endorseKey: Array<string> = []

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userService: UserService,
    private fileChooser: FileChooser,
    private file: File,
    private filePath: FilePath,
    private loadCtrl: LoadingController,
    private toastCtrl: ToastController,
    private usersSVC: UsersService,
    private alertCtrl: AlertController,
    private popoverCtrl: PopoverController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage')
    // //this.urlcvfile = firebase.storage().ref().child('/users/cv/Dian Puspita Chandra_CV.pdf').getDownloadURL()
    // let x = await this.getFileUrl()
    // //let y = Object.prototype(this.urlcvfile)
    // console.log(x)
    this.user = this.userService.getUser()
    this.username = this.user.name
    this.ppimage = this.user.ppimage
    // this.star = this.user.reviews
    this.friends = this.user.friends
    this.friendsnum = Object.keys(this.friends).length

    this.usersSVC.getUsers().subscribe((value: Array<User>) => {
      this.users = value
      this.usersKey = Object.keys(this.users)
      this.usersnum = this.usersKey.length

      this.usersSVC
        .getReviews(firebase.auth().currentUser.uid)
        .subscribe((value: Array<Review>) => {
          this.Review = value
          this.Reviewkey = Object.keys(this.Review)
          this.Reviewnum = Object.keys(this.Review).length

          for (var i = 0; i < this.Reviewnum; i++) {
            this.star = this.star + this.Review[this.Reviewkey[i]].star
            this.Reviewlist = this.Reviewlist || []
            this.Reviewlist.push({
              name: this.Review[this.Reviewkey[i]].name,
              content: this.Review[this.Reviewkey[i]].content,
              ppimage: this.Review[this.Reviewkey[i]].ppimage,
              star: this.Review[this.Reviewkey[i]].star
            })
          }
          this.star = this.star / this.Reviewnum
          if (this.star % 1 != 0) {
            this.starhalf = true
            this.starcounter++
          }
          for (var ix = 1; ix <= 5; ix++) {
            if (this.star - ix >= 0) {
              this.starfull.push(1)
              this.starcounter++
            }
          }
          for (var i = 0; i < 5 - this.starcounter; i++) {
            this.starblank.push(1)
          }
        })

      this.usersSVC
        .getEndorse(firebase.auth().currentUser.uid)
        .subscribe((value: Array<Endorse>) => {
          this.Endorse = value
          this.endorseKey = Object.keys(this.Endorse)
          this.Endorsenum = Object.keys(this.Endorse).length

          for (var i = 0; i < this.Endorsenum; i++) {
            this.Endorselist = this.Endorselist || []
            this.Endorselist.push({
              name: this.Endorse[this.endorseKey[i]].name,
              endorsment: this.Endorse[this.endorseKey[i]].endorsment,
              ppimage: this.Endorse[this.endorseKey[i]].ppimage
            })
          }
        })

      for (var i = 0; i < this.friendsnum; i++) {
        for (var j = 0; j < this.usersnum; j++) {
          if (this.usersKey[j] == this.friends['friend' + (i + 1)]) {
            this.myfriendsdata = this.myfriendsdata || []
            this.myfriendsdata.push({
              key: this.usersKey[j],
              name: this.users[this.usersKey[j]].name,
              friends: this.users[this.usersKey[j]].friends,
              ppimage: this.users[this.usersKey[j]].ppimage
            })
            break
          }
        }
      }
    })
    setTimeout(() => console.log(this.user), 3000)
  }
  ionViewWillEnter() {
    this.section = 'reviews'
  }

  toConnectionProfile(key: string) {
    this.navCtrl.push(ConnectionProfilePage, { key })
  }
  changePic() {}
  changeName() {
    const alert = this.alertCtrl.create({
      title: 'Change Name',
      inputs: [
        {
          name: 'Name',
          placeholder: 'Your Name'
        }
      ],
      buttons: [
        {
          text: 'Change',
          handler: data => {
            if (data.Name != '') {
              this.userService.setUserName(data.Name)
              this.username = data.Name
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel is clicked.')
          }
        }
      ]
    })
    alert.present()
  }
  async _getUser() {
    this.username = this.user.name
  }

  uploadCV() {
    this.fileChooser.open().then(uri => {
      this.filePath
        .resolveNativePath(uri)
        .then(filePath => {
          let dirPathSegments = filePath.split('/')
          let fileName = dirPathSegments[dirPathSegments.length - 1]
          dirPathSegments.pop()
          let dirPath = dirPathSegments.join('/')
          let cvfile = this.username + '_CV.pdf'

          this.file
            .readAsArrayBuffer(dirPath, fileName)
            .then(async buffer => {
              alert('under arraybuffer')
              await this.uploadCVFile(buffer, cvfile)
            })
            .catch(err => {
              alert(err.message)
            })
        })
        .catch(error => {
          alert(error.message)
        })
    })
  }

  toast = this.toastCtrl.create({
    message: 'Your CV has been uploaded',
    duration: 3000,
    position: 'bottom'
  })
  toast2 = this.toastCtrl.create({
    message: 'Failed to upload',
    duration: 3000,
    position: 'bottom'
  })
  loading = this.loadCtrl.create({
    content: 'uploading...'
  })

  async uploadCVFile(buff, fileName) {
    this.loading.present()
    let blob = new Blob([buff], { type: 'application/pdf' })

    let storage = firebase.storage()
    storage
      .ref('/users/cv/' + fileName)
      .put(blob)
      .then(async d => {
        let x = Object.keys(this.getFileUrl(fileName))
        alert(x[1].valueOf)
      })
      .catch(error => {
        this.loading.dismiss()
        alert(error.message)
        this.toast2.present()
        this.ionViewDidLoad()
      })
    //alert(this.urlcvfile)
    this.loading.dismiss()
    this.toast.present()
  }

  async getFileUrl(name: any) {
    firebase
      .storage()
      .ref()
      .child('/users/cv/' + name)
      .getDownloadURL()
      .then(function(url) {
        this.setFile(String(url))
        alert('resolver url ' + UrlResolver.toString)
        return url.valueOf(url)
      })
  }

  setFile(url) {
    this.userService.setCVFile(url)
    alert('setUrl ' + url)
  }

  setImage(url) {
    this.userService.setImage(url)
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage)
    popover.present({
      ev: myEvent
    })
  }

  selectPhoto() {
    this.fileChooser.open().then(uri => {
      this.filePath
        .resolveNativePath(uri)
        .then(filePath => {
          let dirPathSegments = filePath.split('/')
          let fileName = dirPathSegments[dirPathSegments.length - 1]
          dirPathSegments.pop()
          let dirPath = dirPathSegments.join('/')
          let imgfile = this.username + '_Photo.jpg'

          this.file
            .readAsArrayBuffer(dirPath, fileName)
            .then(async buffer => {
              alert('under arraybuffer')
              await this.uploadImgFile(buffer, imgfile)
            })
            .catch(err => {
              alert(err.message)
            })
        })
        .catch(error => {
          alert(error.message)
        })
    })
  }

  toast3 = this.toastCtrl.create({
    message: 'Your picture has been changed',
    duration: 3000,
    position: 'bottom'
  })
  toast4 = this.toastCtrl.create({
    message: 'Failed to change picture',
    duration: 3000,
    position: 'bottom'
  })
  loading2 = this.loadCtrl.create({
    content: 'uploading...'
  })

  async uploadImgFile(buff, fileName) {
    this.loading2.present()
    let blob = new Blob([buff], { type: 'image/jpeg' })

    let storage = firebase.storage()
    storage
      .ref('/users/images/' + fileName)
      .put(blob)
      .then(async d => {
        alert(this.getImgUrl(fileName))
      })
      .catch(error => {
        this.loading2.dismiss()
        alert(error.message)
        this.toast4.present()
        this.ionViewDidLoad()
      })
    //alert(this.urlcvfile)
    this.loading2.dismiss()
    this.toast3.present()
  }

  async getImgUrl(name: any) {
    firebase
      .storage()
      .ref()
      .child('/users/cv/' + name)
      .getDownloadURL()
      .then(function(url) {
        alert(String(url))
        this.setImage(String(url))
        return String(url)
      })
  }
}

import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { ApplySuccessPage } from '../apply-success/apply-success'
import { Job, Company, JobApply } from '../../classes'
import { JobsService } from '../../services/jobs.service'
import moment from 'moment'
import { CompanyService } from '../../services/company.service'
import firebase from 'firebase'
import { JobApplyService } from '../../services/jobApply.service'

/**
 * Generated class for the DetailsVolunteerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details-volunteer',
  templateUrl: 'details-volunteer.html'
})
export class DetailsVolunteerPage {
  loaded: boolean
  company: Company = null
  volunteerJobs: Job = null
  jobKey: string
  isClicked: boolean
  isApplied: boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private jobsService: JobsService,
    private companyService: CompanyService,
    private jobApplyService: JobApplyService
  ) {
    this.jobKey = navParams.get('jobKey')
    this.isApplied = navParams.get('isApplied')
    this.loaded = false
    this.jobsService.getSingleJob(this.jobKey).subscribe((value: Job) => {
      this.volunteerJobs = value
      this.companyService
        .getSingleCompany(this.volunteerJobs.companyID)
        .subscribe((value: Company) => {
          this.company = value
          console.log(this.company)
        })
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsVolunteerPage')
  }
  showDate(date) {
    return moment(date).format('LL')
  }
  showPeriod(date) {
    return moment(date).format('L')
  }
  async onSuccessApply() {
    this.isClicked = true
    await this.jobApplyService.addJobApply(
      new JobApply(firebase.auth().currentUser.uid, 'applied', this.jobKey)
    )
    this.navCtrl.push(ApplySuccessPage)
  }
}

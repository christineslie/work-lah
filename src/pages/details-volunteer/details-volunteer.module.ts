import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailsVolunteerPage } from './details-volunteer';

@NgModule({
  declarations: [
    DetailsVolunteerPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailsVolunteerPage),
  ],
})
export class DetailsVolunteerPageModule {}

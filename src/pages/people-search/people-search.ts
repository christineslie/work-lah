import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular'
import { SearchTabsPage } from '../search-tabs/search-tabs'
import { ConnectionProfilePage } from '../connection-profile/connection-profile'
import { UsersService } from '../../services/users.service'
import { User } from '../../classes'

/**
 * Generated class for the PeopleSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-people-search',
  templateUrl: 'people-search.html'
})
export class PeopleSearchPage {
  keyword: string = ''
  users: Array<User> = []
  userKeys: Array<string> = []
  loaded: Array<Boolean> = []

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private usersService: UsersService
  ) {
    console.log('A')
    this.loaded.fill(false)
    this.viewCtrl = this.navParams.get('viewCtrl')
    this.keyword = this.navParams.get('keyword')
    if (this.keyword != '') {
      console.log(this.keyword)
      this.usersService.getUsers().subscribe((users: any) => {
        console.log(users)
        const allUserKeys = Object.keys(users)
        console.log(allUserKeys)
        const allUsers = users
        console.log(allUsers)
        allUserKeys.forEach(userKey => {
          if (allUsers[userKey].name.toLowerCase().includes(this.keyword.toLowerCase()))
            this.users[userKey] = allUsers[userKey]
        })
        this.userKeys = Object.keys(this.users)
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PeopleSearchPage')
  }

  onInput(event) {
    if (event.target.value.trim() !== '')
      this.navCtrl.push(SearchTabsPage, { keyword: event.target.value.trim() })
  }
  onClickCancel() {
    this.viewCtrl.dismiss()
  }

  toConnectionProfile() {
    this.navCtrl.push(ConnectionProfilePage)
  }

  connectTo() {
    alert('Connect to ...')
  }
}

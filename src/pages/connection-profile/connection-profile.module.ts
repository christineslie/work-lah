import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectionProfilePage } from './connection-profile';

@NgModule({
  declarations: [
    ConnectionProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectionProfilePage),
  ],
})
export class ConnectionProfilePageModule {}

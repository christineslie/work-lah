import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { UsersService } from '../../services/users.service';
import { Observable } from 'rxjs';
import { UserService } from '../../services/user.service';
import { User, Friend, Endorse, Review } from '../../classes';
import { NgForm } from '@angular/forms';
import firebase from 'firebase'

/**
 * Generated class for the ConnectionProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-connection-profile',
  templateUrl: 'connection-profile.html'
})
export class ConnectionProfilePage {
  section: string
  key: string
  userA: User
  user: User
  parentfriends: Array<string>
  currfriends: Array<string>
  name: string
  username: string
  ppimage : string
  star : number = 0
  frientarget: number

  users: Array<User>
  usersnum: number
  usersKey: Array<string> = []

  myfriendsdata: Array<Friend>
  thisfriendsdata: Array<Friend>
  myfriendsnum : number
  thisfriendsnum: number
  isFriend: boolean = false
  endorse: string

  Endorse: Array<Endorse>
  Endorselist: Array<Endorse>
  Endorsenum: number
  endorseKey: Array<string> = []

  
  Review: Array<Review>
  Reviewlist: Array<Review>
  Reviewnum: number
  Reviewkey: Array<string>

  
  starhalf: boolean = false
  starfull: Array<number> = this.starfull || []
  starblank: Array<number> = this.starblank || []
  starcounter: number = 0

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private userService: UserService,
    private usersSVC: UsersService
    ) {
      this.key = this.navParams.get('key')
      this.userA = this.userService.getUser();
      this.parentfriends = this.userA.friends
      this.myfriendsnum = Object.keys(this.parentfriends).length

      this.usersSVC.getEndorse(this.key).subscribe((value: Array<Endorse>)=>{
        this.Endorse = value
        this.endorseKey = Object.keys(this.Endorse)
        this.Endorsenum = Object.keys(this.Endorse).length
        
        for(var i =0; i< this.Endorsenum; i++){
          this.Endorselist = this.Endorselist || []
          this.Endorselist.push({
            name: this.Endorse[this.endorseKey[i]].name,
            endorsment: this.Endorse[this.endorseKey[i]].endorsment,
            ppimage: this.Endorse[this.endorseKey[i]].ppimage
          })
        }
      })
    
      this.usersSVC.getUsers().subscribe((value: Array<User>) => {
        this.users = value
        this.usersKey = Object.keys(this.users)
        this.usersnum = this.usersKey.length
        this.usersSVC.getSingleUser(this.key).subscribe((value : User) => {
          this.user = value
          this.name = this.user.name
          this.currfriends = this.user.friends
          this.ppimage = this.user.ppimage
          this.thisfriendsnum = Object.keys(this.currfriends).length
          this.thisfriendsdata = this.thisfriendsdata

          this.usersSVC.getReviews(this.key).subscribe((value: Array<Review>)=>{
            this.Review = value
            this.Reviewkey = Object.keys(this.Review)
            this.Reviewnum = Object.keys(this.Review).length
            
            for(var i =0; i< this.Reviewnum; i++){
              this.star = this.star + this.Review[this.Reviewkey[i]].star
              this.Reviewlist = this.Reviewlist || []
              this.Reviewlist.push({
                name: this.Review[this.Reviewkey[i]].name,
                content: this.Review[this.Reviewkey[i]].content,
                ppimage: this.Review[this.Reviewkey[i]].ppimage,
                star: this.Review[this.Reviewkey[i]].star
              })
            }
            this.star = this.star/this.Reviewnum
            if(this.star % 1 != 0){
              this.starhalf = true
              this.starcounter++
            }
            for(var ix = 1; ix<=5; ix++){
              if(this.star - ix >= 0){
                this.starfull.push(1)
                this.starcounter++
              }
            }
            for(var i = 0; i < 5 - this.starcounter; i++){
              this.starblank.push(1)
            }
            
          })

          for( let i = 0; i < this.thisfriendsnum; i++){
            for( var j = 0; j < this.usersnum; j++){
              if(this.currfriends['friend'+(i+1)] == this.usersKey[j]){
                this.thisfriendsdata = this.thisfriendsdata || []
                this.thisfriendsdata.push({
                  key: this.usersKey[j],
                  name: this.users[this.usersKey[j]].name,
                  friends: this.users[this.usersKey[j]].friends,
                  ppimage: this.users[this.usersKey[j]].ppimage
                })
                break
              }
            }
          }
        })
        
        for(var i = 0; i < this.myfriendsnum; i++){
          if(this.key == this.parentfriends['friend'+ (i + 1)]){
            this.isFriend = true
          }
        }
        
      }) 
      
      setTimeout(() => console.log(this.user), 3000)
    }

  ionViewDidLoad() {
  }

  toConnectionProfile(key: string) {
    this.navCtrl.push(ConnectionProfilePage, {key})
  }

  ionViewWillEnter() {
    this.section = 'reviews'
  }

  connectTo(key: string) {
    for(var i = 0; i < this.thisfriendsdata.length; i++){
      if(key == this.thisfriendsdata[i].key){
          this.frientarget = Object.keys(this.thisfriendsdata[i].friends).length
        break
      }
    }
    // this.frientarget = 0;
    this.userService.addFriend(this.myfriendsnum+1, key, this.frientarget+1)
  }

  logEndorse(f: NgForm) {
    this.endorse = f.value.desc
    this.userService.addEndorse(this.key, this.endorse)
  }
  
  checkFriend(key: string){
    for(var x = 0; x< this.myfriendsnum; x++){
      if(this.parentfriends['friend'+(x+1)] == key){
        return true
      }
    }
    if(firebase.auth().currentUser.uid == key){
      return true
    }
    return false
  }

  async _getUser() {
    this.username = this.user.name
  }
}

import { Component } from '@angular/core'
import { NavController, LoadingController, AlertController, NavParams } from 'ionic-angular'
import { Geolocation } from '@ionic-native/geolocation'
import { DetailsPartTimePage } from '../details-part-time/details-part-time'
import { SearchTabsPage } from '../search-tabs/search-tabs'
import { DetailsFreelancePage } from '../details-freelance/details-freelance'
import { DetailsVolunteerPage } from '../details-volunteer/details-volunteer'
import firebase from 'firebase'
import { JobsService } from '../../services/jobs.service'
import { Job, LocationCoordinates, Company, JobApply } from '../../classes'
import moment from 'moment'
import { AngularFireDatabase } from '../../../node_modules/angularfire2/database'
import { AngularFireStorage } from '../../../node_modules/angularfire2/storage'
import { CompanyService } from '../../services/company.service'
import { JobApplyService } from '../../services/jobApply.service'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  jobs: Array<Job> = null
  companies: Array<Company> = null
  jobKeys: Array<string> = []
  loading: any
  location: string = ''
  downloadLogo: Array<any> = []
  coordinates: LocationCoordinates = { latitude: 0, longitude: 0 }
  loaded: Array<Boolean> = []
  jobApply: Array<String> = []
  total: any = { foodnBev: 0, writing: 0, techProject: 0 }
  data = {
    coordinates: this.navParams.get('coordinates') || null,
    searchValInput: this.navParams.get('searchVal') || null
  }

  searchVal: number

  constructor(
    public navCtrl: NavController,
    public jobsService: JobsService,
    public loadingCtrl: LoadingController,
    private storage: AngularFireStorage,
    private geolocation: Geolocation,
    private companyService: CompanyService,
    private alertCtrl: AlertController,
    private jobApplyService: JobApplyService,
    public navParams: NavParams
  ) {
    console.log(this.data)
    this.downloadLogo.fill('')
    this.loaded.fill(false)
    this.companyService.getCompanies().subscribe((value: Array<Company>) => {
      this.companies = value
    })
    if (this.navParams.get('searchVal') && this.navParams.get('coordinates')) {
      this.jobsService.getJobs().subscribe((jobs: any) => {
        const input = this.data.searchValInput
        const coordinates = this.data.coordinates
        console.log(input, this.coordinates)
        this.jobs = jobs
        const jobKeys = Object.keys(jobs)
        jobKeys.forEach(jobKey => {
          const singleJob = this.jobs[jobKey]
          const company = this.companies[jobs[jobKey].companyID]
          singleJob.distance = this.distance(
            coordinates.latitude,
            coordinates.longitude,
            company.coordinates.latitude,
            company.coordinates.longitude
          )
        })
        const radius = input == 1 ? 1 : input == 2 ? 3 : input == 3 ? 5 : 10
        console.log(jobs)
        let filteredJobKeys
        filteredJobKeys =
          input != 5
            ? jobKeys.filter(jobKey => {
                return jobs[jobKey].distance < radius
              })
            : jobKeys.filter(jobKey => {
                return jobs[jobKey].distance > radius
              })
        this.jobKeys = filteredJobKeys.sort((a, b) => jobs[a].distance - jobs[b].distance)
        this.jobs = jobs
      })
    } else {
      this.jobsService.getJobs().subscribe((value: Array<Job>) => {
        this.jobs = value
        this.jobKeys = Object.keys(this.jobs)
        this.countTotalJobByCategory()
        console.log(this.jobs)
      })
    }

    this.jobApplyService
      .getJobApplyByUser()
      .subscribe((value: any) => {
        this.jobApply = []
        const jobApplySnapshots = value
        jobApplySnapshots.forEach(jobApplySnapshot => {
          const jobValue = jobApplySnapshot.payload.val()
          if (jobValue.status != 'terminated') this.jobApply.push(jobValue.jobID)
        })
      })
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        console.log(resp)
        this.coordinates.latitude = resp.coords.latitude
        this.coordinates.longitude = resp.coords.longitude
        // fetch(
        //   `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.coordinates.latitude},${
        //     this.coordinates.longitude
        //   }&key=AIzaSyBgbtx9XWmv-Pq8xESAtLfjGMzzn1eEWBw`
        // )
        //   .then(response => {
        //     if (response.ok) {
        //       return response.json()
        //     }
        //     return Promise.reject(response)
        //   })
        //   .then(location => {
        //     console.log(location)
        //     if (location.results.length > 0 && location.plus_code.compound_code) {
        //       console.log(location)
        //       const locationList = location.plus_code.compound_code.split(', ')
        //       let city = locationList[locationList.length - 3] || ''
        //       this.location = city
        //     }
        //   })
      })
      .catch(error => {
        console.log('Error getting location', error)
      })
  }
  getTotalJobByCategory(byCategory: string) {
    return this.total[byCategory]
  }
  countTotalJobByCategory() {
    this.total.foodnBev = 0
    this.total.writing = 0
    this.total.techProject = 0
    this.jobKeys.forEach(jobKey => {
      this.jobs[jobKey].category === 'F&B'
        ? this.total.foodnBev++
        : this.jobs[jobKey].category === 'Writing'
        ? this.total.writing++
        : this.total.techProject++
    })
  }
  isApplied(jobKey: string) {
    return this.jobApply.indexOf(jobKey) === -1 ? false : true
  }

  showDate(date) {
    return moment(date).format('LL')
  }

  onInput(event) {
    if (event.target.value.trim() !== '') {
      this.navCtrl.push(SearchTabsPage, { keyword: event.target.value.trim() })
    }
  }
  ionViewDidLoad() {
    this.searchVal = this.data.searchValInput

    firebase
      .auth()
      .getRedirectResult()
      .then(result => {
        if (firebase.auth().currentUser) {
        } else {
          firebase
            .database()
            .ref('users')
            .child(result.user.uid)
            .set({
              email: result.user.email,
              username: result.user.email
            })
          console.log(result.user)
        }
      })
  }
  onClear(event: Event) {
    return null
  }

  toDetails(jobType, jobKey, isApplied) {
    switch (jobType) {
      case 'part-time':
        this.navCtrl.push(DetailsPartTimePage, { jobKey, isApplied })
        break
      case 'freelance':
        this.navCtrl.push(DetailsFreelancePage, { jobKey, isApplied })
        break
      case 'volunteer':
        this.navCtrl.push(DetailsVolunteerPage, { jobKey, isApplied })
        break
    }
  }

  onClickCategory(byCategory: string) {
    this.navCtrl.push(SearchTabsPage, { byCategory })
  }

  onClickNearMe() {
    let alert = this.alertCtrl.create({
      title: 'Input Location',
      inputs: [
        { type: 'radio', label: '< 1km', value: '1', checked: true },
        { type: 'radio', label: '< 3km', value: '2' },
        { type: 'radio', label: '< 5km', value: '3' },
        { type: 'radio', label: '< 10km', value: '4' },
        { type: 'radio', label: '> 10km', value: '5' }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: input => {
            console.log('Cancel clicked')
          }
        },
        {
          text: 'OK',
          handler: input => {
            this.searchVal = input
            console.log(this.searchVal)
            this.onSubmitNearMe(input)
          }
        }
      ]
    })

    alert.present()
  }

  onSubmitNearMe(input) {
    this.navCtrl.push(HomePage, { coordinates: this.coordinates, searchVal: input })
  }
  distance = (lat1, lon1, lat2, lon2) => {
    var R = 6371
    var dLat = ((lat2 - lat1) * Math.PI) / 180
    var dLon = ((lon2 - lon1) * Math.PI) / 180
    var a =
      0.5 -
      Math.cos(dLat) / 2 +
      (Math.cos((lat1 * Math.PI) / 180) * Math.cos((lat2 * Math.PI) / 180) * (1 - Math.cos(dLon))) /
        2
    return R * 2 * Math.asin(Math.sqrt(a))
  }
}

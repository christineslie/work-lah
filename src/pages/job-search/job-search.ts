import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular'
import { DetailsPartTimePage } from '../details-part-time/details-part-time'
import { SearchTabsPage } from '../search-tabs/search-tabs'
import { DetailsVolunteerPage } from '../details-volunteer/details-volunteer'
import { JobsService } from '../../services/jobs.service'
import moment from 'moment'
import { Job, LocationCoordinates, Company, JobApply } from '../../classes'
import { DetailsFreelancePage } from '../details-freelance/details-freelance'
import { CompanyService } from '../../services/company.service'
import { JobApplyService } from '../../services/jobApply.service'
import firebase from 'firebase'
/**
 * Generated class for the JobSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-job-search',
  templateUrl: 'job-search.html'
})
export class JobSearchPage {
  category: string = ''
  keyword: string = ''
  jobs: Array<Job> = []
  companies: Array<Company> = null
  jobKeys: Array<string> = []
  loaded: Array<Boolean> = []
  coordinates: LocationCoordinates = null
  jobApply: Array<string> = []
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private jobsService: JobsService,
    private companyService: CompanyService,
    private jobApplyService: JobApplyService
  ) {
    this.viewCtrl = this.navParams.get('viewCtrl')
    this.category = this.navParams.get('byCategory')
    this.coordinates = this.navParams.get('coordinates')
    console.log(this.coordinates)
    this.keyword = this.navParams.get('keyword')
    console.log(this.category, this.keyword)
    this.companyService.getCompanies().subscribe((value: any) => {
      this.companies = value
    })
    this.jobApplyService
      .getJobApplyByUser()
      .subscribe((value: any) => {
        this.jobApply = []
        const jobApplySnapshots = value
        jobApplySnapshots.forEach(jobApplySnapshot => {
          this.jobApply.push(jobApplySnapshot.payload.val().jobID)
        })
      })
    if (this.category != '' && this.keyword == '' && this.coordinates == null) {
      this.jobsService.getJobsByCategory(this.category).subscribe((value: any) => {
        value.forEach(job => {
          this.jobs[job.key] = job.payload.val()
        })
        this.jobKeys = Object.keys(this.jobs)
      })
    } else if (this.category == '' && this.keyword != '' && this.coordinates == null) {
      this.jobsService.getListJobs().subscribe((value: any) => {
        value.forEach(job => {
          const jobValue = job.payload.val()
          if (jobValue.role.toLowerCase().includes(this.keyword.toLowerCase()))
            this.jobs[job.key] = jobValue
        })
        this.jobKeys = Object.keys(this.jobs)
      })
    }
  }

  showDate(date) {
    return moment(date).format('LL')
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad JobSearchPage')
  }

  onInput(event) {
    if (event.target.value.trim() !== '') {
      this.navCtrl.push(SearchTabsPage, { keyword: event.target.value.trim() })
    }
  }

  onClickCancel() {
    this.viewCtrl.dismiss()
  }
  isApplied(jobKey: string) {
    return this.jobApply.indexOf(jobKey) === -1 ? false : true
  }
  toDetails(jobType, jobKey, isApplied) {
    switch (jobType) {
      case 'part-time':
        this.navCtrl.push(DetailsPartTimePage, { jobKey, isApplied })
        break
      case 'freelance':
        this.navCtrl.push(DetailsFreelancePage, { jobKey, isApplied })
        break
      case 'volunteer':
        this.navCtrl.push(DetailsVolunteerPage, { jobKey, isApplied })
        break
    }
  }
}

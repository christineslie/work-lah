import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular'
import { HomePage } from '../home/home'
import { AuthService } from '../../services/auth.service'
import { User } from '../../classes'

/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html'
})
export class AuthPage {
  auth: string

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthService
  ) {}

  signIn: { username: string; password: string } = {
    username: '',
    password: ''
  }

  signUp: { email: string; username: string; password: string } = {
    email: '',
    username: '',
    password: ''
  }

  ionViewWillEnter() {
    this.auth = 'sign-in'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage')
  }

  onSignIn() {
    let username: string = this.signIn.username
    let password: string = this.signIn.password
    this.authService.signIn(username, password)
  }

  onSignUp() {
    // alert('Sign up ...')
    let username: string = this.signUp.username
    let password: string = this.signUp.password
    let email = this.signUp.email
    this.authService.signUp(email, username, password)
  }

  onSignInWithGoogle() {
    this.authService.signInWithGoogle()
  }
}

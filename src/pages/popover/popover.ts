import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { FileChooser } from '@ionic-native/file-chooser'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path';
import firebase from 'firebase'
import { AngularFireAuth } from 'angularfire2/auth'
import { UserService } from '../../services/user.service'

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  template:
  `<ion-list>
      <ion-list-header>Edit Image</ion-list-header>
      <button ion-item (click)="camera()">Camera</button>
      <button ion-item (click)="selectPhoto()">Select Photo</button>
    </ion-list>
    `
})
export class PopoverPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private userService: UserService,
    private fileChooser: FileChooser,
    private file: File,
    private filePath: FilePath,
    private loadCtrl: LoadingController,
    private toastCtrl: ToastController,
    private viewCtrl: ViewController
  ) {}
  
  user: {
    email: string, 
    name: string, 
    username: string,
    friends: Array<string>,
    reviews: any,
    endorse: any,
    cvimage: string,
    ppimage: string,
    jobstaken: any
  }
  username: string

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
    this.user = this.userService.getUser()
    this.username = this.user.name
  }

  camera(){

  }

  selectPhoto(){
    this.fileChooser.open().then((uri) =>{
      this.filePath.resolveNativePath(uri).then(filePath => {
        let dirPathSegments = filePath.split('/')
        let fileName = dirPathSegments[dirPathSegments.length-1]
        dirPathSegments.pop()
        let dirPath = dirPathSegments.join('/')
        let imgfile = this.username + '_Photo.jpg'

        this.file.readAsArrayBuffer(dirPath, fileName).then(async (buffer) => {
          alert('under arraybuffer')
          await this.uploadImgFile(buffer, imgfile)
        }).catch((err) => {
          alert(err.message)
        })

      }).catch(error => {
        alert(error.message)
      })

    })
  }

  toast = this.toastCtrl.create({
    message: 'Your picture has been changed',
    duration: 3000,
    position: 'bottom'
  })
  toast2 = this.toastCtrl.create({
    message: 'Failed to change picture',
    duration: 3000,
    position: 'bottom'
  })
  loading = this.loadCtrl.create({
    content: 'uploading...'
  })
    
  async uploadImgFile(buff, fileName){
    this.loading.present()
    let blob = new Blob([buff], {type: 'image/jpeg'})

    let storage = firebase.storage()
    storage.ref('/users/cv/' + fileName).put(blob).then(async d => {
      alert(this.getFileUrl(fileName))
    }).catch((error) => {
      this.loading.dismiss()
      alert(error.message)
      this.toast2.present()
      this.ionViewDidLoad()
    })
    //alert(this.urlcvfile)
    this.loading.dismiss()
    this.toast.present()
  }

  async getFileUrl(name: any){
    firebase.storage().ref().child('/users/cv/' + name).getDownloadURL().then(function(url){
      alert(String(url))
      this.setFile(String(url))
      return String(url)
    })
  }

  setImage(url){
    this.userService.setImage(url)
    this.viewCtrl.dismiss()
  }

}

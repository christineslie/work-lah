export class User {
  email: string
  name: string
  username: string
  friends: Array<string>
  reviews: Array<string>
  endorse: Array<string>
  cvimage: string
  ppimage: string
  jobstaken: any
  constructor(email: string, username: string, name: string, friends: Array<string>, reviews: Array<string>, endorse: Array<string>, cvimage: string, ppimage: string, jobstaken: any) {
    this.email = email
    this.name = name
    this.username = username
    this.friends = friends
    this.reviews = reviews
    this.endorse = endorse
    this.cvimage = cvimage
    this.ppimage = ppimage
    this.jobstaken = jobstaken
  }
}

export class Friend {
  key: string
  name: string
  friends: Array<string>
  ppimage: string
  constructor(key: string, name: string, friends: Array<string>, ppimage: string) {
    this.key = key
    this.name = name
    this.friends = friends
    this.ppimage = ppimage
  }
}
export class Review {
  star: number
  name: string
  content: string
  ppimage: string
  constructor(star: number, name: string, content: string, ppimage: string) {
    this.star = star
    this.name = name
    this.content = content
    this.ppimage = ppimage
  }
}
export class Endorse {
  name: string
  endorsment: string
  ppimage: string
  constructor(name: string, endorsment: string, ppimage: string) {
    this.name = name
    this.endorsment = endorsment
    this.ppimage = ppimage
  }
}

export class LocationCoordinates {
  latitude: number
  longitude: number
  constructor(latitude: number, longitude: number) {
    this.latitude = latitude
    this.longitude = longitude
  }
}

export class JobApply {
  userID: string
  status: 'applied' | 'accepted' | 'terminated' | ''
  jobID: string
  constructor(userID: string, status: 'applied' | 'accepted' | 'terminated' | '', jobID: string) {
    this.userID = userID
    this.status = status
    this.jobID = jobID
  }
}

export class Company {
  name: string
  logo: string
  email: string
  location: string
  coordinates: LocationCoordinates
  constructor(
    name: string,
    logo: string,
    email: string,
    location: string,
    coordinates: LocationCoordinates
  ) {
    this.name = name
    this.logo = logo
    this.email = email
    this.location = location
    this.coordinates = coordinates
  }
}
export class Job {
  category: string
  companyID: string
  datePosted: string
  description: string
  responsibilities: Array<string>
  role: string
  salary: number
  schedules: Schedules
  type: string
  constructor(
    category: string,
    companyID: string,
    datePosted: string,
    description: string,
    responsibilities: Array<string>,
    role: string,
    salary: number,
    schedules: Schedules,
    type: string
  ) {
    this.category = category
    this.companyID = companyID
    this.datePosted = datePosted
    this.description = description
    this.responsibilities = responsibilities
    this.role = role
    this.salary = salary
    this.schedules = schedules
    this.type = type
  }
}

export class Schedules {
  shiftDay: { [day: string]: boolean }
  shiftTime: Array<{ timeIn: string; timeOut: string }>
}

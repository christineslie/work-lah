/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict'

const functions = require('firebase-functions')
const nodemailer = require('nodemailer')
const admin = require('firebase-admin')
const serviceAccount = require('./service-account.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://work-lah.firebaseio.com/'
})

const gmailEmail = functions.config().gmail.email
const gmailPassword = functions.config().gmail.password
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
})

const APP_NAME = 'Worklah'

/**
 * Sends a email to the company.
 */
exports.sendApplyJobOffer = functions.database.ref(`/jobApply/{jobKey}`).onCreate(applyJob => {
  const email = 'worklah.team@gmail.com'
  const jobID = applyJob.val().jobID
  const userID = applyJob.val().userID
  const jobRef = admin.database().ref(`/jobs/${jobID}`)
  const userRef = admin.database().ref(`/users/${userID}`)
  let job
  let user
  let company
  return jobRef.once('value').then(snapshot => {
    job = snapshot.val()
    userRef.once('value').then(snapshot => {
      user = snapshot.val()
      admin
        .database()
        .ref(`/companies/${job.companyID}`)
        .once('value')
        .then(snapshot => {
          company = snapshot.val()
          sendCompanyEmail(email, job, user, company)
        })
    })
  })
})

function sendCompanyEmail(email, job, user, company) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@worklah.com>`,
    to: email
  }
  let currency = job.type == 'part-time' ? `per hour` : job.type == 'volunteer' ? '-' : '/content'
  let type =
    job.type == 'part-time' ? `Part Time` : job.type == `volunteer` ? `Volunteer` : `Freelance`
  mailOptions.subject = `${APP_NAME}: Applicant Request`
  mailOptions.html =
    `<div> Dear Sir, <br><br>${user.name} is applying for a job in your company (${
      company.companyName
    }) as a ${job.role}. Applicant details can be found below. <br><br>` +
    `Full Name: ${user.name}<br>` +
    `Email: ${user.email}<br>` +
    `Type: ${type}<br>` +
    `Salary: ${job.salary}$ ` +
    `${currency}` +
    `<br><br>` +
    `You can get ${user.name}'s CV from the attachment or <a href="${
      user.cvimage
    }"> this link</a> to help you choose the best ${
      job.role
    }.<br><br>Best Regards,<br><strong>Worklah Team</strong></div>`
  mailOptions.attachments = [
    {
      filename: `CV_${user.name}_${company.companyName}.pdf`,
      path: `${user.cvimage}`,
      contentType: 'application/pdf'
    }
  ]
  return mailTransport.sendMail(mailOptions).then(() => {
    return console.log('New welcome email sent to:', email)
  })
}
